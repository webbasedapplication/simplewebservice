package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/*
 * Our Entity-Object has to be annotated with @Entity to be visible for JPA.
 * There is special @Table annotation where we can explicitly define which table in the DB fits to our class.
 * If no @Table annotation is provided, JPA assumes the class name eq. table name.
 */
@Entity
public class Todo {

    /*
     * Every Entity-class has to define a unique ID
     * We let JPA care about its uniqueness with the @GeneratedValue annotation
     */
    @Id
    @GeneratedValue
    private Integer id;

    /*
     * All fields annotated with @NotNull have to be set when sending JSON-Object to our webservice, representing a Todo instance
     */
    @NotNull
    private String title;

    @NotNull
    private String description;

    public Todo(Integer id, String title, String description){
        this.id = id;
        this.title = title;
        this.description = description;
    }

    /*
     * we need an empty constructor for JSON-mapping and JPA (ORM)
     */
    protected Todo(){}

    /*
     * We can find all fields in our JSON-response having a getter-method defined!
     */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*
     * It is strongly recommended to override hasCode() & equals() method of an Entity working with JPA!
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equals(id, todo.id) &&
                Objects.equals(title, todo.title) &&
                Objects.equals(description, todo.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description);
    }

    /*
     * toString to have a nice Logging output for our Entity
     */
    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

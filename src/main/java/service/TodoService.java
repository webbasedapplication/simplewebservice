package service;

import entities.Todo;
import repository.TodoRepository;

import javax.inject.Inject;
import java.util.logging.Logger;

/*
 * Kind of middleware between our REST-Controller and repository. We encapsulate repository access to be able to
 * handle various exception that might occure. We don't want to deliver HTTP-status 500 and stacktraces to our clients.
 */
public class TodoService {

    private static final Logger LOGGER = Logger.getLogger(TodoService.class.getName());

    /*
     * We want CDI (TomcatEE) to take care about object instantiation
     */
    @Inject
    private TodoRepository todoRepository;

    /*
     * For all methods here we are accessing our DB via repository, we catch all eventual exceptions that might occur
     */
    public Boolean saveTodo(Todo todo){
        try {
            todoRepository.persistTodo(todo);
            return true;
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return false;
        }
    }

}

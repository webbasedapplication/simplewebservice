package controller;

import entities.Todo;
import service.TodoService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.logging.Logger;

@Path("/api/todo") // We define a Basepath
@Produces(MediaType.APPLICATION_JSON) // We can define a global MediaType or per Method with @Produces
public class Controller {

    // creating a Logger is always a good thing
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    @Inject
    private TodoService todoService;

    /*
     *    GET-Method with Queryparams URL: http://localhost:8080/api/todo?name=<a-Name>
     *    If no name is passed we can provide a default one with Annotation @DefaultValue("DefaultName")
     */
    @GET
    public Todo getTodo(@QueryParam("todoId") @DefaultValue("0") Integer id){
        Todo todo = new Todo(id,"Brush my car", "Make it clean");
        return todo;
    }

    /*
     *    GET-Method with Path params URL: http://localhost:8080/api/todo/<a-Name>
     */
    @GET
    @Path("/{todoId}")
    public Todo getTodoWithPathParam(@PathParam("todoId") Integer id){
        Todo todo = new Todo(id,"Brush my car", "Make it clean");
        return todo;
    }

    @GET
    @Path("/all")
    public ArrayList<Todo> getAll(){
        ArrayList<Todo> todos = new ArrayList<>();
        todos.add(new Todo(1,"Brush my car", "Make it clean"));
        todos.add(new Todo(2,"Cooke me something", "Make some bacon and egg"));
        return todos;
    }

    /*
     *    POST-Method URL: http://localhost:8080/api/todo
     *    We are accepting JSON as in the @Consumes Annotation defined. The JSON Body is mapped to Todo Entity.
     *    @Valid ensures that all properties in Todo that are marked with @NotNullable are not null
     *    Since save-method in our service returns true or false, we'll send an appropriate response code to our clients
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTodo(@Valid Todo todo){
        LOGGER.info("Creating Todo: "+todo);
        Boolean success = todoService.saveTodo(todo);
        Response.Status response = success ? Response.Status.OK : Response.Status.BAD_REQUEST;
        return Response.status(response).build();
    }

}

package repository;

import entities.Todo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;


/*
 * Repositories are responsible for DB-interaction. They encapsulate all DB-access!
 */
public class TodoRepository {

    /*
     * Our DB-access are JTA-managed, which means our runtime on TomcatEE will take care about DB-connections eg write transactions
     * in case of an error we have a build in rollback mechanism.
     */
    @PersistenceContext(unitName = "todo")
    private EntityManager entityManager;

    /*
     * Write access to DB must have the @Transactional annotation. We use the persist()-method for inserts and merge()-method for updates.
     */
    @Transactional
    public void persistTodo(Todo todo){
        entityManager.persist(todo);
    }

    /*
     * JPQL example, equivalent in SQL is "SELECT * FROM Todo" (assuming the table name we query is "Todo")
     */
    public List<Todo> findAll(){
        Query query = entityManager.createQuery("SELECT t FROM Todo t");
        return query.getResultList();
    }

}
